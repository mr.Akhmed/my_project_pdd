from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework_simplejwt.views import TokenRefreshView

from apps.profiles import serializers
from apps.profiles.models import Profile
from apps.profiles.serializers import UserProfileSerializer, UserSerializer
from apps.profiles.services import UserService


@extend_schema(tags=["РЕГИСТРАЦИЯ"])
@extend_schema_view(
    post=extend_schema(
        summary='РЕГИСТРАЦИЯ ПО ИМЕНИ',
    ),
)
class RegistrationAPIView(generics.CreateAPIView):
    serializer_class = serializers.RegistrationSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user = UserService.create_user(
                username=serializer.validated_data['username']
            )
            token = UserService.get_token(user)
            return Response(
                data={
                    'user': user.username,
                    'refresh': str(token),
                    'access': str(token.access_token),

                }, status=status.HTTP_201_CREATED
            )

        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@extend_schema(tags=["ПРОФИЛЬ"])
@extend_schema_view(
    get=extend_schema(
        summary="ПРОСМОТР ПРОФИЛЯ ПОЛЬЗОВАТЕЛЯ",
    ),
    patch=extend_schema(
        summary='ИЗМЕНЕНИЕ ИМЕНИ ПОЛЬЗОВАТЕЛЯ',
    ),
    delete=extend_schema(
        summary='УДАЛЕНИЕ АККАУНТА ПОЛЬЗОВАТЕЛЯ'
    )
)
class ProfileStatsView(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]
    http_method_names = ['get', 'patch', 'delete']

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return UserProfileSerializer
        else:
            return UserSerializer

    def get_object(self):
        try:
            return Profile.objects.get(user=self.request.user)
        except Profile.DoesNotExist:
            raise ValueError('Пользователь не найден')

    def retrieve(self, request, *args, **kwargs):
        obj = self.get_object()
        data = self.get_serializer(obj).data
        return Response(data=data, status=status.HTTP_200_OK)

    def patch(self, request, *args, **kwargs):
        user = self.request.user
        serializer = self.get_serializer(user, data=request.data, partial=True)
        if serializer.is_valid():
            data = serializer.validated_data.get('username', user.username)
            user.username = data
            user.save()
            return Response(data=serializer.data, status=status.HTTP_200_OK)
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request, *args, **kwargs):
        user = request.user
        user.delete()
        return Response(
            data={
                "message": 'Пользователь был удален!',
            }, status=status.HTTP_410_GONE
        )


@extend_schema(tags=["РЕГИСТРАЦИЯ"])
@extend_schema_view(
    post=extend_schema(
        summary='ОБНОВЛЕНИЕ ACCESS ТОКЕНА',
    ),
)
class RefreshTokenToAccess(TokenRefreshView):
    pass
