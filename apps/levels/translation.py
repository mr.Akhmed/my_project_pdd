from modeltranslation.translator import register, TranslationOptions

from .models import *


@register(Level)
class LevelTranslation(TranslationOptions):
    fields = ('name', 'description')
    required_languages = {'ru': ('name',), 'ky': ('name',)}


@register(Question)
class QuestionTranslation(TranslationOptions):
    fields = ('title', 'explanation',)
    required_languages = {'ru': ('title',), 'ky': ('title',)}


@register(Answer)
class AnswerTranslation(TranslationOptions):
    fields = ('text',)
    required_languages = ('ru', 'ky')


@register(Lesson)
class LessonTranslation(TranslationOptions):
    fields = ('title',)
    required_languages = ('ru', 'ky')


@register(Test)
class TestTranslation(TranslationOptions):
    fields = ('title',)
    required_languages = ('ru', 'ky')
