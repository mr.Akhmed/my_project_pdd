from django.urls import path

from apps.levels import views

urlpatterns = [
    path('level/', views.LevelListApiView.as_view()),

    path('level/<int:level_pk>/lessons/', views.LessonsListView.as_view()),

    path('level/<int:level_pk>/test/', views.TestAPIView.as_view()),

    path('level/<int:level_pk>/lesson/<int:lesson_pk>/',
         views.LessonQuestionsListAPIView.as_view()),

    path('level/<int:level_pk>/lesson/<int:lesson_pk>/finish/',
         views.FinishLessonAPIView.as_view()),

    path('level/<int:level_pk>/test/<int:test_pk>/', views.TestLevelView.as_view()),

    path('level/<int:level_pk>/test/<int:test_pk>/finish/',views.FinishTestAPIView.as_view()),

    path('level/<int:level_pk>/test/<int:test_pk>/reset/',views.ResetTestProgressAPIView.as_view()),

    path('level/<int:level_pk>/lesson/<int:lesson_pk>/reset/',views.ResetLessonProgressAPIView.as_view()),
]
