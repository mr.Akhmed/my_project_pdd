from django.db import models

from apps.profiles.models import User

DEFAULT_DESCRIPTION_RU = ("Привет! Меня зовут Степи.Я очень хорошо знаю "
                          "ПДД и хочу научить тебя! Давай учиться вместе!")
DEFAULT_DESCRIPTION_KY = ("Салам! Менин атым Степи.Жол эрежесин жакшы билем "
                          "жана мен сага үйрөткүм келет! Келгиле, чогуу окуйлу!")


class Level(models.Model):
    """
        Модель для уровня
    """
    name = models.CharField(max_length = 50, verbose_name = 'Название уровня')
    description = models.CharField(
        max_length = 255, verbose_name = 'Описание уровня',
        blank = True, null = True
    )
    order = models.PositiveIntegerField(
        verbose_name = 'Порядковый номер уровня',
        blank = True,
        null = True
    )

    class Meta:
        verbose_name = "уровни и уроки"
        verbose_name_plural = "Добавление уровней и уроков"

    def save(self, *args, **kwargs):
        self.description_ru = self.description_ru if self.description_ru else DEFAULT_DESCRIPTION_RU
        self.description_ky = self.description_ky if self.description_ky else DEFAULT_DESCRIPTION_KY
        if not self.order:
            self.order = Level.objects.count() + 1
        super(Level, self).save(*args, **kwargs)

    def __str__(self):
        return f'Уровень - {self.order}'


class Question(models.Model):
    """
        Модель для вопроса
    """
    lesson = models.ForeignKey(
        'Lesson', verbose_name = 'Урок',
        on_delete = models.CASCADE,
        related_name = 'questions',
    )
    image = models.ImageField(
        upload_to = 'level/question_image/', null = True, blank = True, verbose_name = 'Изображение'
    )
    title = models.CharField(max_length = 255, verbose_name = 'Вопрос')
    explanation = models.CharField(
        max_length = 255, verbose_name = 'Пояснение к вопросу',
        null = True, blank = True
    )
    order = models.PositiveIntegerField(
        verbose_name = 'Порядковый номер вопроса', blank = True, null = True
    )

    class Meta:
        verbose_name = "вопросы и ответы"
        verbose_name_plural = "Добавление вопросов и ответов"

    def save(self, *args, **kwargs):
        if not self.order:
            self.order = Question.objects.filter(lesson = self.lesson).count() + 1
        super(Question, self).save(*args, **kwargs)

    def __str__(self):
        return f'Урок - {self.lesson.order} - Вопрос - {self.order}'


class Answer(models.Model):
    """
        Модель для ответа
    """
    question = models.ForeignKey(
        'Question', on_delete = models.CASCADE, related_name = 'answers',
        verbose_name = 'Вопрос'
    )
    text = models.CharField(max_length = 255, verbose_name = 'Ответ')
    is_correct = models.BooleanField(default = False, choices = ((True, 'Правильный'), (False, 'Неправильный')))

    class Meta:
        verbose_name = "ответ"
        verbose_name_plural = "Добавить ответы"

    def __str__(self):
        return f'{self.text[:50]}'


class UserLevelProgress(models.Model):
    """
        Прогресс пользователя в уровнях
    """
    user = models.ForeignKey(User, on_delete = models.CASCADE, related_name = 'user_level_progress')
    level = models.ForeignKey('Level', on_delete = models.CASCADE)
    passed = models.BooleanField(default = False)

    class Meta:
        unique_together = ('user', 'level')
        verbose_name = "Прогресс по уровню"
        verbose_name_plural = "Прогресс по уровням"


class BaseUserProgress(models.Model):
    """
        Базовая модель прогресса пользователя
    """
    user_level_progress = models.ForeignKey(
        UserLevelProgress, on_delete = models.CASCADE,
        related_name = '%(class)s',
    )
    passed = models.BooleanField(default = False)
    streak = models.PositiveIntegerField(default = 0)
    correct_answers = models.PositiveIntegerField(default = 0)
    wrong_answers = models.PositiveIntegerField(default = 0)

    class Meta:
        abstract = True


class UserLessonProgress(BaseUserProgress):
    """
        Прогресс пользователя в уроках
    """
    lesson = models.ForeignKey('Lesson', on_delete = models.CASCADE, related_name = 'user_lesson_progress')

    class Meta:
        unique_together = (('lesson', 'user_level_progress'),)
        verbose_name = 'прогресс по уроку'
        verbose_name_plural = 'прогресс по урокам'


class UserQuestionProgress(models.Model):
    user_lesson_progress = models.ForeignKey(
        'UserLessonProgress', on_delete = models.CASCADE,
        related_name = 'user_answer_progress'
    )
    question = models.ForeignKey('Question', on_delete = models.CASCADE)

    def __str__(self):
        return self.user_lesson_progress.user_level_progress.user.username


class UserTestProgress(BaseUserProgress):
    """
        Прогресс пользователя в тестах
    """
    final_test = models.ForeignKey('Test', on_delete = models.CASCADE, related_name = 'user_test_progress')

    class Meta:
        unique_together = ('user_level_progress', 'final_test')
        verbose_name = 'прогресс по тесту'
        verbose_name_plural = 'прогресс по тестам'


class UserTestQuestionProgress(models.Model):
    user_test_progress = models.ForeignKey(
        'UserTestProgress', on_delete = models.CASCADE,
        related_name = 'user_answer_progress'
    )
    question = models.ForeignKey('Question', on_delete = models.CASCADE)

    def __str__(self):
        return self.user_test_progress.user_level_progress.user.username


class Lesson(models.Model):
    level = models.ForeignKey(
        'Level', verbose_name = 'Уровень',
        on_delete = models.CASCADE, related_name = '%(class)s'
    )
    order = models.PositiveIntegerField(
        verbose_name = 'Порядковый номер урока', blank = True, null = True
    )
    title = models.CharField(max_length = 50, verbose_name = 'Название урока')
    """
        Модель урока
    """

    def save(self, *args, **kwargs):
        if not self.order:
            self.order = Lesson.objects.filter(level = self.level).count() + 1
        super(Lesson, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'урок'
        verbose_name_plural = 'Редактирование и просмотр уроков'

    def __str__(self):
        return f'Уровень {self.level.order} - Урок {self.order} '


class Test(models.Model):
    """
        Финальный тест
    """
    level = models.OneToOneField('Level', on_delete = models.CASCADE)
    title = models.CharField(max_length = 50, verbose_name = 'Название теста')
    questions = models.ManyToManyField(
        'Question', verbose_name = 'Список вопросов',
        related_name = 'tests', blank = True
    )

    def get_random_questions(self, questions_count = 20):
        all_questions = self.questions.filter(lesson__level = self.level).order_by('?')
        return all_questions[:questions_count]

    class Meta:
        verbose_name = 'тест'
        verbose_name_plural = 'Добавление тестов'

    def __str__(self):
        return f'Уровень {self.level.order} - ТестID {self.pk}'
