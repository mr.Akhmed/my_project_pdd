from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import AdminPasswordChangeForm, UserChangeForm, BaseUserCreationForm
from django.contrib.auth.models import Group, Permission
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _

from .models import *


class GroupAdmin(admin.ModelAdmin):
    """ Для скрытия Group в админ панели """

    def has_module_permission(self, request):
        return False


admin.site.unregister(Group)

Permission.__str__ = lambda self: '%s' % (self.name)


class AddUserForm(BaseUserCreationForm):
    email = forms.EmailField(required = True, label = _("Email"), )

    class Meta:
        model = User
        fields = ('email',)


@admin.register(User)
class UserAdmin(UserAdmin):
    actions_on_top = False
    add_form_template = "admin/auth/user/add_form.html"
    change_user_password_template = None
    fieldsets = (
        (None, {"fields": ("username", "email", "password")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "user_permissions",
                ),
            },
        ),
    )
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": ("username", "email", "password1", "password2"),
            },
        ),
    )
    list_display = ('id', 'username', 'email', 'is_staff', 'is_active')
    add_form = AddUserForm
    form = UserChangeForm
    search_fields = ("username", "email")
    change_password_form = AdminPasswordChangeForm


@admin.register(Rank)
class RankAdmin(admin.ModelAdmin):
    fields = ('title_ru', 'title_ky', 'point_required', 'order',)
    list_display_links = ('id', 'title_ru',)
    list_display = ('id', 'title_ru', 'title_ky', 'point_required', 'cumulative_sum', 'order')
    readonly_fields = ('order',)
    ordering = ('-order',)


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    fields = ('user', 'get_image', 'completed_lesson', 'completed_test', 'total_points', 'rank')
    list_display = ('id', 'user', 'completed_lesson', 'completed_test', 'total_points', 'rank')
    list_display_links = ('id', 'user')

    readonly_fields = ('user', 'get_image', 'completed_lesson', 'completed_test', 'total_points', 'rank')

    def has_add_permission(self, request):
        return False

    def get_image(self, obj):
        return mark_safe(f'<img src={obj.image.url} width="150" height="150">')

    get_image.short_description = 'Фото пользователя'


admin.site.register(Group, GroupAdmin)
