from django.apps import AppConfig


class StaticsAndRatingsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.statistics_and_ratings'
