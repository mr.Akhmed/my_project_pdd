from .models import Category, Video, SubCategory
from modeltranslation.translator import register, TranslationOptions


@register(Category)
class CategoryTranslation(TranslationOptions):
    fields = ('title',)
    required_languages = ('ru', 'ky')


@register(SubCategory)
class SubCategoryTranslation(TranslationOptions):
    fields = ('title',)
    required_languages = ('ru', 'ky')


@register(Video)
class VideoTranslation(TranslationOptions):
    fields = ('title', 'video_url')
    required_languages = ('ru', 'ky')
