from datetime import timedelta

from config.settings.base import *

SECRET_KEY = config('SECRET_KEY_PROD')

DEBUG = config('DEBUG_PROD', cast=bool, default=False)

ALLOWED_HOSTS = config('ALLOWED_HOSTS_PROD', cast=list)

MIDDLEWARE += ['corsheaders.middleware.CorsMiddleware']

# DATABASES = {
#     'default': {
#         'ENGINE': config('POSTGRES_ENGINE'),
#         'NAME': config('POSTGRES_DB'),
#         'USER': config('POSTGRES_USER'),
#         'PASSWORD': config('POSTGRES_PASSWORD'),
#         'HOST': config('POSTGRES_HOST'),
#         'PORT': config('POSTGRES_PORT'),
#     }
# }

CORS_ALLOWED_ORIGINS = [
    "http://209.38.228.54:82",
    "http://209.38.228.54",
    "http://209.38.228.54:8000",
]

CSRF_TRUSTED_ORIGINS = [
    "http://209.38.228.54:82",
    "http://209.38.228.54",
    "http://209.38.228.54:8000",
]

SIMPLE_JWT = {
    "ACCESS_TOKEN_LIFETIME": timedelta(weeks=365 * 100),
    "REFRESH_TOKEN_LIFETIME": timedelta(weeks=365 * 100),
    "SIGNING_KEY": SECRET_KEY,
    "AUTH_HEADER_TYPES": ("JWT",),
}

STATIC_URL = 'back_static/'
STATIC_ROOT = BASE_DIR / 'back_static/'

MEDIA_URL = 'back_media/'
MEDIA_ROOT = BASE_DIR / 'back_media/'
