from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers
from apps.profiles.models import Profile


class ProfileSortingSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField()
    temp_rank = serializers.IntegerField(read_only=True)
    rank = serializers.StringRelatedField()
    is_current_user = serializers.SerializerMethodField()

    class Meta:
        model = Profile
        fields = ('id', 'user', 'image', 'rank', 'total_points', 'completed_lesson',
                  'completed_test', 'temp_rank', 'is_current_user')

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_is_current_user(self, obj):
        # Здесь предполагается, что контекст сериализатора содержит запрос (request), в котором есть пользователь
        request = self.context.get('request', None)
        if request and hasattr(request, 'user'):
            return obj.user == request.user
        return False
