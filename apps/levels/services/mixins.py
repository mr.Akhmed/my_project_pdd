from apps.levels import constants
from apps.levels.models import Answer


class IsAvailableMixin:
    """
        Миксин для проверки доступа пользователя к УРОВНЮ/УРОКУ
    """

    @classmethod
    def is_lesson_or_level_available(cls, obj, list_passed):
        """
            Проверка доступен ли урок или уровень
        """
        return int(obj.order) in constants.DEFAULT_AVAILABLE_LESSONS or (
                list_passed is not None and int(obj.order) - 1 in list_passed
        )


class AnswerMixin:
    """
        Миксин - процесс прохождения урока
    """
    model = Answer

    @classmethod
    def get_answer(cls, **kwargs):
        try:
            return cls.model.objects.select_related('question').get(**kwargs)
        except cls.model.DoesNotExist:
            raise ValueError('id ответа или вопроса не существует')

    @classmethod
    def is_correct_update(cls, user_progress):
        user_progress.correct_answers += 1
        user_progress.streak += 1
        user_progress.save()

    @classmethod
    def update_is_streak(cls, profile, user_progress, points):
        profile.total_points += int(points)
        profile.point_count += int(points)
        user_progress.streak = 0
        user_progress.save()
        profile.save()

    @classmethod
    def update_is_wrong(cls, user_progress):
        user_progress.wrong_answers += 1
        user_progress.streak = 0
        user_progress.save()
