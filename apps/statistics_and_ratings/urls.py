from django.urls import path

from apps.statistics_and_ratings.views import SortedUsersAPIView

urlpatterns = [
    path('top-ratings/', SortedUsersAPIView.as_view()),
]