from rest_framework_simplejwt.tokens import RefreshToken
from apps.levels.models import User


class UserService:
    model = User

    @classmethod
    def create_user(cls, username):
        return cls.model.objects.create_user(username=username)

    @classmethod
    def get_token(cls, user):
        return RefreshToken.for_user(user)

