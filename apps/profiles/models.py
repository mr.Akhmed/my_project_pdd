import os
import random

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.db.models.functions import Rank
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from apps.profiles.usermanager import CustomUserManager


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.
    Username and password are required. Other fields are optional.
    """
    username_validator = UnicodeUsernameValidator()

    username = models.CharField(
        _("username"),
        max_length=32,
        help_text=_(
            "Required. 32 characters or fewer. Letters, digits and @/./+/-/_ only."
        ),
    )
    email = models.EmailField(
        _("email address"), unique=True, blank=True, null=True
    )
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_(
            "Designates whether the user can log into this admin site."
        ),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)

    objects = CustomUserManager()

    EMAIL_FIELD = "email"
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ['username']

    class Meta:
        verbose_name = 'пользователя'
        verbose_name_plural = _("users")

    def __str__(self):
        return self.username


def get_first_default_rank():
    """return first rank by order"""
    try:
        default_rank_id = Rank.objects.get(point_required=0).pk
        return default_rank_id
    except Rank.DoesNotExist:
        return None


def set_random_photo():
    image_dir = 'back_media/default_profile_images/'
    image_files = [f for f in os.listdir(image_dir) if os.path.isfile(os.path.join(image_dir, f))]
    if not image_files:
        return None
    random_image = random.choice(image_files)
    return os.path.join('default_profile_images', random_image)


class Profile(models.Model):
    """Profile of user"""
    user = models.OneToOneField(
        'User', on_delete=models.CASCADE, related_name='profile'
    )
    image = models.ImageField(
        upload_to='profile/profile_pics',
        default=set_random_photo,
        blank=True,
        null = True
    )

    # stats
    completed_lesson = models.PositiveIntegerField(
        default=0, verbose_name='Завершенные уроки'
    )
    completed_test = models.PositiveIntegerField(
        default=0, verbose_name='Завершенные тесты'
    )

    # score
    total_points = models.PositiveIntegerField(default=0, verbose_name='Баллы')
    point_count = models.PositiveIntegerField(default=0, verbose_name='Счетчик баллов для профиля')
    rank = models.ForeignKey(
        'Rank', models.SET_NULL, 'users_rank',
        null=True, blank=True, default=get_first_default_rank,
        verbose_name='Ранг',
    )

    class Meta:
        verbose_name = 'профиль'
        verbose_name_plural = 'Профили пользователей'

    def __str__(self):
        return f'{self.user.username}'

    def get_next_rank(self):
        return (Rank.objects.filter(order__gt=self.rank.order)
                .order_by('point_required')).first()

    def update_rank(self):
        ranks = Rank.objects.order_by('point_required')
        if self.rank:
            ranks = ranks.filter(order__gt=self.rank.order)
        for rank in ranks:
            if self.total_points >= rank.cumulative_sum:
                self.point_count = 0
                self.rank = rank

    def save(self, *args, **kwargs):
        self.update_rank()
        super().save(*args, **kwargs)


def get_ordinal_order():
    return int(Rank.objects.count() + 1)


class Rank(models.Model):
    """ Model for rank of user"""
    title = models.CharField(max_length=64, verbose_name='Название ранга')
    point_required = models.PositiveIntegerField(
        verbose_name='Необходимые баллы к общему количеству', unique=True
    )
    order = models.PositiveIntegerField(
        default=get_ordinal_order, verbose_name='Порядковый номер ранга',
        blank=True
    )
    cumulative_sum = models.PositiveIntegerField(default=0, verbose_name='Общее количество баллов')

    class Meta:
        verbose_name = 'ранг'
        verbose_name_plural = 'Добавление рангов'

    def __str__(self):
        return self.title
