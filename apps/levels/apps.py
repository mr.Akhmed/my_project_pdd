from django.apps import AppConfig


class LevelsConfig(AppConfig):  # Переименовано для ясности
    default_auto_field = 'django.db.models.BigAutoField'

    def ready(self):
        import apps.levels.signals

    name = 'apps.levels'
    verbose_name = 'Прохождение уроков'
