from django.urls import path

from apps.profiles import views

urlpatterns = [
    path('registration/', views.RegistrationAPIView.as_view(), name='username-registration'),
    path('profile/', views.ProfileStatsView.as_view(), name='profile'),
    path('token/refresh/', views.RefreshTokenToAccess.as_view(), name='refresh_token')
]
