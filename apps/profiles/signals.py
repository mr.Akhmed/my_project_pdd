from django.db.models import Window, Sum, F
from django.db.models.signals import post_save, pre_delete, post_delete
from django.dispatch import receiver

from apps.profiles.models import User, Profile, Rank


# triggerd when User object is created


@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(
            user=instance
        )


@receiver(post_delete, sender=Rank)
@receiver(post_save, sender=Rank)
def update_rank_order(sender, instance, **kwargs):
    ranks = Rank.objects.annotate(
        cum_sum=Window(
            expression=Sum('point_required'),
            order_by=F('point_required').asc(),
        )
    )

    for index, rank in enumerate(ranks, start=1):
        Rank.objects.filter(pk=rank.pk).update(order=index, cumulative_sum=rank.cum_sum)
