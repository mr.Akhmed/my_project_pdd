from django.db.models import Count, Prefetch

from apps.levels.models import Level, UserLessonProgress, UserLevelProgress


class LevelService:
    model = Level
    lesson_progress_model = UserLessonProgress
    level_progress_model = UserLevelProgress

    @classmethod
    def get_level_queryset(cls, user):
        return cls.model.objects.prefetch_related(
            Prefetch(
                'userlevelprogress_set__userlessonprogress',
                queryset = cls.lesson_progress_model.objects.filter(
                    user_level_progress__user = user, passed = True
                ),
                to_attr = 'user_lessons',
            )
        ).annotate(num_lessons = Count('lesson')).order_by('order')

    @classmethod
    def filter_progress(cls, **kwargs):
        return (cls.level_progress_model.objects.select_related('level').filter(**kwargs)
                .values_list('level__order', flat = True))
