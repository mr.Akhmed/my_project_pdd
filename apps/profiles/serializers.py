from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers
from apps.profiles.models import Profile, User, Rank


class RegistrationSerializer(serializers.Serializer):
    """ username serializer """
    username = serializers.CharField(max_length=32)


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username',)


class RankSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rank
        fields = ('title',)


class UserProfileSerializer(serializers.ModelSerializer):
    user = serializers.StringRelatedField()
    rank = serializers.StringRelatedField()
    next_rank = serializers.SerializerMethodField()
    points_to_next_rank = serializers.SerializerMethodField()

    class Meta:
        model = Profile
        fields = ("user image completed_lesson completed_test total_points rank "
                  "next_rank point_count points_to_next_rank ").split()

    @extend_schema_field(OpenApiTypes.STR)
    def get_next_rank(self, profile):
        try:
            return profile.get_next_rank().title
        except:
            return None

    @extend_schema_field(OpenApiTypes.INT)
    def get_points_to_next_rank(self, profile):
        try:
            return profile.get_next_rank().point_required
        except:
            return None
