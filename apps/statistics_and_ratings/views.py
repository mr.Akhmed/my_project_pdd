from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from apps.statistics_and_ratings.serializers import (
    ProfileSortingSerializer
)

from apps.statistics_and_ratings.utils import get_top_profiles_with_current_user


@extend_schema(tags=["ТОП РЕЙТИНГ"])
@extend_schema_view(
    get=extend_schema(
        summary="ПОЛУЧЕНИЯ СПИСКА ТОП 20 ПОЛЬЗОВАТЕЛЕЙ ",
    ),
)
class SortedUsersAPIView(generics.ListAPIView):
    serializer_class = ProfileSortingSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        user = self.request.user
        return get_top_profiles_with_current_user(user)
