from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import generics

from apps.video_library.models import (Category,
                                       Video
                                       )
from apps.video_library.serializers import (CategorySerializer,
                                            VideoSerializer
                                            )


@extend_schema(tags=["ВИДЕО"])
@extend_schema_view(
    get=extend_schema(
        summary='ПОЛУЧЕНИЕ КАТЕГОРИЙ И СПИСОК ПОДКАТЕГОРИЙ',
        description='Возвращает **список категорий и список их подкатегорий**'
    )
)
class CategoryView(generics.ListAPIView):
    """ Просмотр список категорий """
    queryset = Category.objects.prefetch_related('subcategories').all()
    serializer_class = CategorySerializer


@extend_schema(tags=["ВИДЕО"])
@extend_schema_view(
    get=extend_schema(
        summary='ПОЛУЧЕНИЕ ВИДЕО ОТ ПОДКАТЕГОРИИ',
        description='Эта API возвращает **список видео** для \n'
                    'определенной **подкатегории**, определенной по **id** в URL.'
    )
)
class SubCategoryVideosView(generics.ListAPIView):
    serializer_class = VideoSerializer

    def get_queryset(self):
        """
        Этот view должен возвращать список видео для
        определенной подкатегории, определенной по id в URL.
        """
        subcategory_id = self.kwargs['id']
        return Video.objects.select_related('subcategory').filter(subcategory_id=subcategory_id)
