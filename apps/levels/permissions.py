from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import BasePermission

from apps.levels.models import UserLessonProgress, Lesson, UserLevelProgress, Level
from apps.levels.services.mixins import IsAvailableMixin


class IsLessonAvailable(BasePermission):
    """
        Permission для проверки доступа пользователя к уроку
    """
    message = 'Вам недоступен этот урок! Пройдите ваши доступные уроки'

    def has_permission(self, request, view):
        user = request.user

        list_lesson_progress = UserLessonProgress.objects.filter(
            user_level_progress__level_id=view.kwargs['level_pk'],
            user_level_progress__user=user,
            passed=True,
        ).values_list('lesson__order', flat=True)
        try:
            current_lesson = getattr(view, 'current_lesson', None)
            if current_lesson is None:
                current_lesson = Lesson.objects.get(
                    id=view.kwargs['lesson_pk'],
                    level_id=view.kwargs['level_pk'],
                )
                setattr(view, 'current_lesson', current_lesson)
        except Lesson.DoesNotExist:
            return PermissionDenied('Урок не найден')
        if user.is_authenticated:
            return IsAvailableMixin.is_lesson_or_level_available(current_lesson, list_lesson_progress)


class IsLevelAvailable(BasePermission):
    """
        Permission для проверки доступа пользователя к уровню
    """
    message = 'Вам недоступен этот уровень! Пройдите ваши доступные уроки'

    def has_permission(self, request, view):
        user = request.user
        level_pk = view.kwargs['level_pk']
        try:
            current_level = getattr(view, 'current_level', None)
            if current_level is None:
                current_level = Level.objects.get(id=level_pk)
                setattr(view, 'current_level', current_level)
        except Level.DoesNotExist:
            return ValueError('Уровень не найден!')

        list_level_progress = UserLevelProgress.objects.filter(
            user=request.user,
            passed=True,
        ).values_list('level__order', flat=True)

        if user.is_authenticated:
            return IsAvailableMixin.is_lesson_or_level_available(current_level, list_level_progress)


class IsTestAvailable(BasePermission):
    """
    Permission для проверки доступа пользователя к тесту
    """
    message = 'Вам недоступен этот тест! Для доступа к нему пройдите все уроки'

    def has_permission(self, request, view):
        user = request.user

        level_pk = view.kwargs['level_pk']
        user_progress_count = getattr(view, 'user_progress_count', None)
        lessons_count = getattr(view, 'lessons_count', None)
        if user_progress_count is None:
            user_progress_count = UserLessonProgress.objects.filter(
                user_level_progress__level_id=level_pk,
                user_level_progress__user=user,
                passed=True,
            ).count()
            setattr(view, 'user_progress_count', user_progress_count)
        if lessons_count is None:
            lessons_count = Lesson.objects.filter(level_id=level_pk).count()
            setattr(view, 'lessons_count', lessons_count)

        return user_progress_count >= lessons_count
