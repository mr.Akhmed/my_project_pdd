import re

from django.db import models
from django.utils.translation import gettext_lazy as _


class BaseModel(models.Model):
    """ Абстрактный базовый класс для общих полей """
    title = models.CharField(max_length = 255, verbose_name = _('Название'))

    class Meta:
        abstract = True


class Category(BaseModel):
    """ Категории """

    def __str__(self):
        return str(self.title)

    class Meta(BaseModel.Meta):
        verbose_name = _('категорию')
        verbose_name_plural = _('Категории')


class SubCategory(BaseModel):
    """ Подкатегории """
    category = models.ForeignKey(
        Category, on_delete = models.CASCADE, related_name = 'subcategories',
        verbose_name = _("Категория")
    )
    image = models.ImageField(upload_to = "video_library/category_image/", verbose_name = _('Изображение'))

    def __str__(self):
        return f"{self.title}"

    class Meta(BaseModel.Meta):
        verbose_name = _('Подкатегорию')
        verbose_name_plural = _('Подкатегории')


class Video(BaseModel):
    """ Видеоуроки """

    subcategory = models.ForeignKey(
        SubCategory, on_delete = models.CASCADE, related_name = 'videos',
        verbose_name = _("Подкатегория")
    )
    video_url = models.URLField(verbose_name = _("Ссылка на видео"))

    def get_image_url(self):
        if video_1 := re.search(r"youtube\.com/watch\?v=([^&]+)", self.video_url):
            return f"http://img.youtube.com/vi/{video_1.group(1)}/0.jpg"

        elif video_2 := re.search(r"youtu\.be/([^&]+)", self.video_url):
            return f"http://img.youtube.com/vi/{video_2.group(1)}/0.jpg"
        return None

    def __str__(self):
        return f"{self.title}"

    class Meta(BaseModel.Meta):
        verbose_name = _('видеоуроки')
        verbose_name_plural = _('Видеоуроки')
