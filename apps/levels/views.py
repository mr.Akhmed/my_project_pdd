from drf_spectacular.utils import extend_schema, extend_schema_view
from rest_framework import status
from rest_framework.generics import ListAPIView, RetrieveAPIView, GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.levels import serializers
from apps.levels.permissions import IsLessonAvailable, IsLevelAvailable, IsTestAvailable
from apps.levels.services.lesson import (LessonService, LessonQuestionService, FinishLessonService,
                                         ResetLessonProgressService)
from apps.levels.services.level import LevelService
from apps.levels.services.test import TestService, TestQuestionService, FinishTestService, ResetTestProgressService


@extend_schema(tags = ["УРОВНИ"])
@extend_schema_view(
    get = extend_schema(
        summary = 'ПОЛУЧЕНИЯ СПИСКА УРОВНЕЙ',
        description = 'Возвращает список всех **уровней**'
    ),
)
class LevelListApiView(ListAPIView):
    """
    Предоставляет список всех уровней
    """
    http_method_names = ['get']
    serializer_class = serializers.SerializerListLevel
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return LevelService.get_level_queryset(user = self.request.user)

    def list(self, request, *args, **kwargs):
        list_passed_levels = LevelService.filter_progress(
            user = self.request.user,
            passed = True
        )
        lesson_data = self.get_serializer(
            self.get_queryset(), many = True,
            context = {'list_passed_levels': list_passed_levels}
        ).data
        return Response(data = lesson_data, status = status.HTTP_200_OK)


@extend_schema(tags = ["УРОКИ"])
@extend_schema_view(
    get = extend_schema(
        summary = 'ПОЛУЧЕНИЯ СПИСКА УРОКОВ',
        description = 'Возвращает **список всех уроков** по **id уровня** в URL'
    ),
)
class LessonsListView(ListAPIView):
    http_method_names = ['get']
    serializer_class = serializers.SerializerListOfLessons
    permission_classes = [IsAuthenticated, IsLevelAvailable]

    def get_queryset(self):
        queryset = LessonService.filter_lesson(level_id = self.kwargs['level_pk'])
        return queryset

    def list(self, request, *args, **kwargs):
        try:
            user_level_progress, created = LessonService.get_or_create(
                user = self.request.user,
                level_id = self.kwargs['level_pk'],
            )
        except LessonService.progress_model.DoesNotExist:
            raise ValueError('Уровень не был найден')

        list_passed_lessons = LessonService.filter_progress(
            userlevelprogress = user_level_progress,
            passed = True,
        )

        data = self.get_serializer(
            self.get_queryset(), many = True,
            context = {'list_passed_lessons': list_passed_lessons}
        ).data
        return Response(data = data, status = status.HTTP_200_OK)


@extend_schema(tags = ["ТЕСТЫ"])
@extend_schema_view(
    get = extend_schema(
        summary = 'ПОЛУЧЕНИЯ ТЕСТА',
        description = 'Возвращает **тест** по **id уровня** в URL'
    ),
)
class TestAPIView(RetrieveAPIView):
    lookup_field = 'level_pk'
    http_method_names = ['get']
    serializer_class = serializers.TestSerializer
    permission_classes = [IsAuthenticated, IsLevelAvailable]

    def get_object(self):
        object = TestService.get_test(level_id = self.kwargs['level_pk'])
        return object

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        user = self.request.user

        test_progress = TestService.get_test_progress(
            final_test = instance,
            user_level_progress__user = user,
            passed = True
        )

        list_passed_lessons = TestService.filter_lesson_progress(
            user_level_progress__user = user,
            user_level_progress__level_id = self.kwargs['level_pk'],
            passed = True
        )

        data = self.get_serializer(
            instance,
            context = {
                'list_passed_lessons': list_passed_lessons,
                'test_progress': test_progress
            }
        ).data

        return Response(data = data, status = status.HTTP_200_OK)


@extend_schema(tags = ["УРОКИ"])
@extend_schema_view(
    get = extend_schema(
        summary = 'ПОЛУЧЕНИЯ СПИСКА ВОПРОСОВ С ОТВЕТАМИ ',
    ),
    post = extend_schema(
        request = serializers.SerializerAnswerValidate,
        responses = serializers.ResponseAnswering,
        summary = 'ПРОХОЖДЕНИЕ УРОКА',
        description = 'Возращает правильный ли ответ пользователя \\ '
                      'количество правильных и неправильных ответов, и серию правильных ответов \\ '
                      'и статус TRUE or FALSE , если пользователь ответил на 5 вопросов подряд!'
    )
)
class LessonQuestionsListAPIView(ListAPIView):
    http_method_names = ['get', 'post']
    permission_classes = [IsAuthenticated, IsLessonAvailable]

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return serializers.SerializerListQuestion
        elif self.request.method == 'POST':
            return serializers.SerializerAnswerValidate

    def get_queryset(self):
        questions = LessonQuestionService.filter_question(
            lesson__level_id = self.kwargs['level_pk'],
            lesson_id = self.kwargs['lesson_pk']
        )
        return questions

    def post(self, request, *args, **kwargs):
        user = self.request.user
        user_level_progress, created = LessonQuestionService.get_or_create_level_progress(
            user = user,
            level_id = self.kwargs['level_pk']
        )
        user_lesson_progress, created = LessonQuestionService.get_or_create_lesson_progress(
            user_level_progress = user_level_progress,
            lesson_id = self.kwargs['lesson_pk']
        )
        profile = LessonQuestionService.get_profile(user_lesson_progress)
        list_answered = LessonQuestionService.filter_answer_progress(
            user_lesson_progress = user_lesson_progress
        )
        serializer = self.get_serializer(data = request.data)
        if serializer.is_valid():
            answer = LessonQuestionService.get_answer(
                id = serializer.validated_data['answer_id'],
                question_id = serializer.validated_data['question_id'],
            )
            if answer.question.id not in list_answered:
                LessonQuestionService.create_question_progress(
                    user_lesson_progress = user_lesson_progress,
                    question = answer.question
                )
                if answer.is_correct:
                    LessonQuestionService.is_correct_update(user_lesson_progress)
                    if user_lesson_progress.streak >= 5:
                        LessonQuestionService.update_is_streak(profile, user_lesson_progress, points = 5)
                        response_serializer = serializers.ResponseAnswering(
                            user_lesson_progress, context = {'answer': answer}
                        )
                        return Response(
                            data = response_serializer.data,
                            status = status.HTTP_200_OK
                        )
                    response_serializer = serializers.ResponseAnswering(
                        user_lesson_progress, context = {'answer': answer}
                    )
                    return Response(
                        data = response_serializer.data,
                        status = status.HTTP_200_OK
                    )
                LessonQuestionService.update_is_wrong(user_lesson_progress)
                response_serializer = serializers.ResponseAnswering(
                    user_lesson_progress, context = {'answer': answer}
                )
                return Response(
                    data = response_serializer.data, status = status.HTTP_200_OK
                )
            return Response(data = {'message': 'Вы уже ответили на этот вопрос'})
        return Response(data = serializer.errors, status = status.HTTP_400_BAD_REQUEST)


@extend_schema(tags = ["УРОКИ"])
@extend_schema_view(
    post = extend_schema(
        summary = 'ЗАВЕРШЕНИЯ УРОКА',
        description = 'Когда пользователь ответил на все вопросы отправляется GET запрос \\ '
                      'API возвращает ответ пройден урок или нет. Если да, то возвращается \\ '
                      'его статистика кол. правильных ответов, неправильных, кол.баллов \\ '
    )
)
class FinishLessonAPIView(GenericAPIView):
    serializer_class = serializers.FinishLessonSerializer

    def post(self, request, *args, **kwargs):
        userlessonprogress = FinishLessonService.get_lesson_progress(
            user_level_progress__user = self.request.user,
            user_level_progress__level_id = self.kwargs['level_pk'],
            lesson = self.kwargs['lesson_pk']
        )
        question_progress = FinishLessonService.filter_question_progress(user_lesson_progress = userlessonprogress)
        question_count = FinishLessonService.question_count(lesson_id = self.kwargs['lesson_pk'])
        data = self.get_serializer(
            userlessonprogress,
            context = {
                'question_progress': question_progress,
                'question_count': question_count
            }
        ).data
        profile = userlessonprogress.user_level_progress.user.profile
        if not userlessonprogress.passed:
            if data['passed']:
                FinishLessonService.update_first_passed(profile)
        elif userlessonprogress.passed:
            if data['passed']:
                FinishLessonService.update_re_passing(profile)
        FinishLessonService.finish_update(userlessonprogress, question_progress)
        return Response(data = data, status = status.HTTP_200_OK)


@extend_schema(tags = ["ТЕСТЫ"])
@extend_schema_view(
    get = extend_schema(
        summary = 'ПОЛУЧЕНИЯ СПИСКА ВОПРОСОВ С ОТВЕТАМИ',
    ),
    post = extend_schema(
        request = serializers.SerializerAnswerValidate,
        responses = serializers.ResponseTestAnswering,
        summary = 'ПРОХОЖДЕНИЕ ТЕСТА',
        description = 'Возращает правильный ли ответ пользователя \\ '
                      'количество правильных и неправильных ответов, и серию правильных ответов \\ '
                      'и статус TRUE or FALSE , если пользователь ответил на 5 вопросов подряд!'
    )
)
class TestLevelView(RetrieveAPIView):
    lookup_field = 'test_pk'
    http_method_names = ['get', 'post']
    permission_classes = [IsAuthenticated, IsTestAvailable]

    def get_object(self):
        return TestQuestionService.get_test(level_id = self.kwargs['level_pk'], id = self.kwargs['test_pk'])

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return serializers.TestQuestionSerializer
        else:
            return serializers.SerializerAnswerValidate

    def post(self, request, *args, **kwargs):
        user = request.user
        user_level_progress, created = TestQuestionService.get_or_create_level_progress(
            user = user,
            level_id = self.kwargs['level_pk']
        )
        user_test_progress, created = TestQuestionService.get_or_create_test_progress(
            user_level_progress = user_level_progress,
            final_test_id = self.kwargs['test_pk']
        )
        profile = user_test_progress.user_level_progress.user.profile
        list_answered = TestQuestionService.filter_question_progress(user_test_progress = user_test_progress)

        serializer = self.get_serializer(
            data = request.data
        )
        if serializer.is_valid():
            answer = TestQuestionService.get_answer(
                id = serializer.validated_data['answer_id'],
                question_id = serializer.validated_data['question_id'],
            )
            if answer.question.id not in list_answered:
                TestQuestionService.create_question_progress(
                    user_test_progress = user_test_progress,
                    question = answer.question
                )
                if answer.is_correct:
                    TestQuestionService.is_correct_update(user_test_progress)
                    if user_test_progress.streak >= 5:
                        TestQuestionService.update_is_streak(profile, user_test_progress, points = 5)
                        response_serializer = serializers.ResponseAnswering(
                            user_test_progress, context = {'answer': answer}
                        )
                        return Response(
                            data = response_serializer.data,
                            status = status.HTTP_200_OK
                        )
                    response_serializer = serializers.ResponseAnswering(
                        user_test_progress, context = {'answer': answer}
                    )
                    return Response(
                        data = response_serializer.data,
                        status = status.HTTP_200_OK
                    )
                TestQuestionService.update_is_wrong(user_test_progress)
                response_serializer = serializers.ResponseAnswering(
                    user_test_progress, context = {'answer': answer}
                )
                return Response(
                    data = response_serializer.data, status = status.HTTP_200_OK
                )
            return Response(data = {'message': 'Вы уже ответили на этот вопрос'})
        return Response(data = serializer.errors, status = status.HTTP_400_BAD_REQUEST)


@extend_schema(tags = ["ТЕСТЫ"])
@extend_schema_view(
    post = extend_schema(
        summary = 'ЗАВЕРШЕНИЯ ТЕСТА',
        description = 'Когда пользователь ответил на все вопросы отправляется GET запрос \\ '
                      'API возвращает ответ пройден тест или нет. Если да, то возвращается \\ '
                      'его статистика кол. правильных ответов, неправильных, кол.баллов \\ '
    )
)
class FinishTestAPIView(GenericAPIView):
    serializer_class = serializers.FinishTestSerializer

    def post(self, request, *args, **kwargs):
        usertestprogress = FinishTestService.get_test_progress(
            user_level_progress__user = self.request.user,
            user_level_progress__level_id = self.kwargs['level_pk'],
            final_test_id = self.kwargs['test_pk']
        )
        question_progress = FinishTestService.filter_question_progress(user_test_progress = usertestprogress)
        question_count = FinishTestService.question_count(id = self.kwargs['test_pk'])
        data = self.get_serializer(
            usertestprogress,
            context = {
                'question_progress': question_progress,
                'question_count': question_count
            }
        ).data
        profile = usertestprogress.user_level_progress.user.profile
        if not usertestprogress.passed:
            if data['passed']:
                FinishTestService.update_first_passed(profile, usertestprogress)
        elif usertestprogress.passed:
            if data['passed']:
                FinishTestService.update_re_passing(profile)
        FinishTestService.finish_update(usertestprogress, question_progress)
        return Response(
            data = data, status = status.HTTP_200_OK
        )


@extend_schema(tags = ["УРОКИ"])
@extend_schema_view(
    post = extend_schema(
        summary = 'СБРОС ПРОГРЕССА ПО УРОКАМ',
    )
)
class ResetLessonProgressAPIView(APIView):
    def post(self, request, *args, **kwargs):
        user = self.request.user
        userlessonprogress = ResetLessonProgressService.get_lesson_progress(
            user_level_progress__user = user,
            lesson_id = self.kwargs['lesson_pk'],
            lesson__level_id = self.kwargs['level_pk']
        )
        userquestionprogress = ResetLessonProgressService.filter_question_progress(
            user_lesson_progress__user_level_progress__user = user
        )
        ResetLessonProgressService.reset_progress(userquestionprogress, userlessonprogress)
        return Response(data = {'is_reset': True}, status = status.HTTP_200_OK)


@extend_schema(tags = ["ТЕСТЫ"])
@extend_schema_view(
    post = extend_schema(
        summary = 'СБРОС ПРОГРЕССА ПО ТЕСТАМ',
    )
)
class ResetTestProgressAPIView(APIView):
    def post(self, request, *args, **kwargs):
        user = self.request.user
        usertestprogress = ResetTestProgressService.get_test_progress(
            user_level_progress__user = user,
            final_test__level_id = self.kwargs['level_pk'],
            final_test_id = self.kwargs['test_pk']
        )
        userquestionprogress = ResetTestProgressService.filter_question_progress(
            user_test_progress__user_level_progress__user = user
        )
        ResetTestProgressService.reset_progress(usertestprogress,userquestionprogress)
        return Response(data = {'is_reset': True}, status = status.HTTP_200_OK)
