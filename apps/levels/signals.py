import os
from io import BytesIO

from PIL import Image
from django.core.files.base import ContentFile
from django.db.models.signals import post_delete, pre_save, pre_delete
from django.dispatch import receiver

from apps.levels.models import *


@receiver(post_delete, sender = Level)
def update_level_order(sender, instance, **kwargs):
    levels = Level.objects.all()
    for index, level in enumerate(levels, start = 1):
        Level.objects.filter(pk = level.pk).update(order = index)


@receiver(post_delete, sender = Lesson)
def update_lesson_order(sender, instance, **kwargs):
    lessons = Lesson.objects.filter(level = instance.level)
    for index, lesson in enumerate(lessons, start = 1):
        Lesson.objects.filter(pk = lesson.pk).update(order = index)


@receiver(post_delete, sender = Question)
def update_question_order(sender, instance, **kwargs):
    questions = Question.objects.filter(lesson = instance.lesson)
    for index, question in enumerate(questions, start = 1):
        Question.objects.filter(pk = question.pk).update(order = index)


@receiver(pre_delete, sender = Question)
def image_model_delete(sender, instance, **kwargs):
    if instance.image.name:
        instance.image.delete(False)


@receiver(pre_save, sender = Question)
def image_compress(sender, instance, **kwargs):
    if instance.image:
        im = Image.open(instance.image)
        im = im.convert('RGB')

        im = im.resize((1366, 768), Image.Resampling.LANCZOS)

        im_io = BytesIO()
        im.save(im_io, 'JPEG', quality = 90, optimize = True)

        filename, extension = os.path.splitext(instance.image.name)
        new_filename = f"{filename}.jpg"
        new_image = ContentFile(im_io.getvalue(), name = new_filename)
        instance.image = new_image
