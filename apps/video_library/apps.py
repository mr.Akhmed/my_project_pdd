from django.apps import AppConfig


class VideoLibraryConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.video_library'
    verbose_name = 'Библиотека видеоуроков'

    def ready(self):
        import apps.video_library.signals