from datetime import timedelta
from config.settings.base import *
from decouple import config


SECRET_KEY = config('SECRET_KEY')
DEBUG = config('DEBUG', cast=bool)
PRODUCTION = config('PRODUCTION', cast=bool)
INSTALLED_APPS += ["debug_toolbar"]


ALLOWED_HOSTS = ["localhost", "127.0.0.1"]
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}

MIDDLEWARE += [
    "debug_toolbar.middleware.DebugToolbarMiddleware",
    "querycount.middleware.QueryCountMiddleware"
]

SIMPLE_JWT = {
    "ACCESS_TOKEN_LIFETIME": timedelta(weeks=365 * 100),
    "REFRESH_TOKEN_LIFETIME": timedelta(weeks=365 * 100),
    "SIGNING_KEY": SECRET_KEY,
    "AUTH_HEADER_TYPES": ("JWT",),
}
INTERNAL_IPS = [
    # ...
    "127.0.0.1",
    # ...
]

QUERYCOUNT = {
    'THRESHOLDS': {
        'MEDIUM': 50,
        'HIGH': 200,
        'MIN_TIME_TO_LOG': 0,
        'MIN_QUERY_COUNT_TO_LOG': 0
    },
    'IGNORE_REQUEST_PATTERNS': [],
    'IGNORE_SQL_PATTERNS': [],
    'DISPLAY_DUPLICATES': None,
    'RESPONSE_HEADER': 'X-DjangoQueryCount-Count'
}

QUERYCOUNT = {
    'DISPLAY_DUPLICATES': 5,
}
