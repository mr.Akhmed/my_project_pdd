from rest_framework import serializers
from apps.video_library.models import Category, Video, SubCategory


class SubCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SubCategory
        fields = ['id', 'title', 'image', 'category']


class CategorySerializer(serializers.ModelSerializer):
    subcategories = SubCategorySerializer(many=True, read_only=True)

    class Meta:
        model = Category
        fields = ['id', 'title', 'subcategories']


class VideoSerializer(serializers.ModelSerializer):
    image_url = serializers.SerializerMethodField()

    class Meta:
        model = Video
        fields = ("id", "title", 'video_url', 'subcategory', 'image_url',)

    def get_image_url(self, obj):
        return obj.get_image_url()
