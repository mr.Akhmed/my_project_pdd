from PIL import Image
from django.core.files.base import ContentFile
from django.db.models.signals import pre_delete, pre_save
from django.dispatch import receiver
import os
from io import BytesIO

from apps.video_library.models import SubCategory


@receiver(pre_delete, sender=SubCategory)
def image_model_delete(sender, instance, **kwargs):
    if instance.image.name:
        instance.image.delete(False)


@receiver(pre_save, sender=SubCategory)
def image_compress(sender, instance, **kwargs):
    if instance.image:
        im = Image.open(instance.image)
        im = im.convert('RGB')

        im = im.resize((240, 240), Image.Resampling.LANCZOS)

        im_io = BytesIO()
        im.save(im_io, 'JPEG', quality=90, optimize=True)

        filename, extension = os.path.splitext(instance.image.name)
        new_filename = f"{filename}.jpg"
        new_image = ContentFile(im_io.getvalue(), name=new_filename)
        instance.image = new_image
