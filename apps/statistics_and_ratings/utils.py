from django.db.models.functions import RowNumber
from apps.profiles.models import Profile
from django.db.models.expressions import Window
from django.db.models import F


def get_top_profiles_with_current_user(user):
    profiles = Profile.objects.annotate(
        temp_rank=Window(
            expression=RowNumber(),
            order_by=[F('total_points').desc(), F('completed_lesson').desc(), F('completed_test').desc()]
        )
    ).order_by('temp_rank')

    profile_ranks = {profile.id: profile.temp_rank for profile in profiles}

    top_profile_ids = list(profile_ranks.keys())[:20]

    current_user_profile_id = user.profile.id
    if current_user_profile_id not in top_profile_ids:
        top_profile_ids.append(current_user_profile_id)

    top_profiles = Profile.objects.filter(id__in=top_profile_ids).select_related("user").order_by('-total_points',
                                                                                                  '-completed_lesson',
                                                                                                  '-completed_test')

    for profile in top_profiles:
        profile.temp_rank = profile_ranks[profile.id]

    return top_profiles


def get_sorted_profiles():
    # Извлечение всех профилей, отсортированных по заданным критериям
    profiles = Profile.objects.select_related('user').order_by('-total_points', '-completed_lesson', '-completed_test')

    # Расчёт временного ранга для каждого профиля
    temporary_ranks = {profile.id: rank for rank, profile in enumerate(profiles, start=1)}

    # Присвоение временного ранга каждому профилю
    for profile in profiles:
        profile.temporary_rank = temporary_ranks[profile.id]

    # Возвращаем список профилей с присвоенными временными рангами
    return profiles
