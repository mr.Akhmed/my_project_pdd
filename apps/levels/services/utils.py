def calculate_lesson_completion_percentage(correct_answers, wrong_answers):
    total_questions = correct_answers + wrong_answers
    if total_questions > 0:
        return (correct_answers / total_questions) * 100
    else:
        return 0


def determine_completion_status(percentage):
    if percentage >= 80:
        return 'Отлично'
    elif percentage >= 51:
        return 'Хорошо'
    elif percentage >= 0:
        return 'Неплохо'
    else:
        return 'Ошибка вычисления'
