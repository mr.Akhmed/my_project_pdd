from drf_spectacular.types import OpenApiTypes
from drf_spectacular.utils import extend_schema_field
from rest_framework import serializers

from apps.levels.models import Level, Question, Answer, Lesson, Test, UserLessonProgress
from apps.levels.services.mixins import IsAvailableMixin


class SerializerListLevel(serializers.ModelSerializer):
    """
    Сериализатор для уровня
    """
    passed_lessons = serializers.SerializerMethodField()
    points = serializers.SerializerMethodField()
    available = serializers.SerializerMethodField()

    class Meta:
        model = Level
        fields = 'id name description order passed_lessons points available'.split()

    @extend_schema_field(
        {
            'example': {
                "user_lessons": "integer",
                "level_lessons": "integer"
            },
        }
    )
    def get_passed_lessons(self, obj):
        return {
            'user_lessons': int(sum(len(level.user_lessons) for level in obj.userlevelprogress_set.all())),
            'level_lessons': int(obj.num_lessons)
        }

    @extend_schema_field(
        {
            'example': {
                "user_points": "integer",
                "level_points": "integer"
            },
        }
    )
    def get_points(self, obj):
        passed_lessons = self.get_passed_lessons(obj)
        return {
            'user_points': passed_lessons['user_lessons'] * 15,
            'level_points': passed_lessons['level_lessons'] * 15
        }

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_available(self, obj):
        list_passed = self.context.get('list_passed_levels')
        return IsAvailableMixin.is_lesson_or_level_available(obj, list_passed)


class SerializerAnswers(serializers.ModelSerializer):
    """
        Сериализатор для ответов
    """

    class Meta:
        model = Answer
        fields = 'id text is_correct'.split()


class SerializerListOfLessons(serializers.ModelSerializer):
    """
        Сериализатор для списка уроков
    """
    available = serializers.SerializerMethodField()
    is_passed = serializers.SerializerMethodField()
    points = serializers.SerializerMethodField()
    is_lesson = serializers.SerializerMethodField()
    level_name = serializers.SerializerMethodField()

    class Meta:
        model = Lesson
        fields = 'id title order available is_passed points is_lesson level_name'.split()

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_available(self, obj):
        list_passed = self.context.get('list_passed_lessons')
        return IsAvailableMixin.is_lesson_or_level_available(obj, list_passed)

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_is_passed(self, obj):
        list_passed = self.context.get('list_passed_lessons')
        if list_passed:
            if obj.order in list_passed:
                return True
        return False

    @extend_schema_field(
        {
            'example': {
                'user_points': 'integer',
                'lesson_points': 'integer'
            }
        }
    )
    def get_points(self, obj):
        list_passed = self.context.get('list_passed_lessons')
        return {
            'user_points': 15 if obj.order in list_passed else 0,
            'lesson_points': 15
        }

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_is_lesson(self, obj):
        return True

    @extend_schema_field(OpenApiTypes.STR)
    def get_level_name(self, obj):
        return obj.level.name


class SerializerListQuestion(serializers.ModelSerializer):
    """
        Сериализатор для вопросов
    """
    answers = SerializerAnswers(many=True)

    class Meta:
        model = Question
        fields = 'id image explanation title order answers'.split()


class TestSerializer(serializers.ModelSerializer):
    available = serializers.SerializerMethodField()
    is_test = serializers.SerializerMethodField()
    is_passed = serializers.SerializerMethodField()
    points = serializers.SerializerMethodField()

    class Meta:
        model = Test
        fields = 'id title available is_passed points is_test'.split()

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_available(self, obj):
        list_passed = self.context['list_passed_lessons']
        return obj.lesson_counts == len(list_passed)

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_is_test(self, obj):
        return True

    @extend_schema_field(
        {
            'example': {
                'user_points': 'ingeger',
                'test_points': 'integer'
            }
        }
    )
    def get_points(self, obj):
        test_progress = self.context.get('test_progress')
        user_points = 0
        if test_progress:
            user_points = 30
        max_points = 30
        return {
            'user_points': user_points,
            'test_points': max_points
        }

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_is_passed(self, obj):
        test_progress = self.context['test_progress']
        if test_progress:
            return test_progress.passed
        return False


class SerializerAnswerValidate(serializers.Serializer):
    """
        Сериализатор для ответов.
            Проверка ответа на верность
    """

    question_id = serializers.IntegerField(required=True)
    answer_id = serializers.IntegerField(required=True)


class ResponseAnswering(serializers.ModelSerializer):
    is_correct = serializers.SerializerMethodField()
    correct_answers = serializers.SerializerMethodField()
    wrong_answers = serializers.SerializerMethodField()
    is_streak = serializers.SerializerMethodField()
    streak = serializers.SerializerMethodField()
    is_answered = serializers.SerializerMethodField()
    explanation = serializers.SerializerMethodField()

    class Meta:
        model = UserLessonProgress
        fields = 'is_correct correct_answers wrong_answers streak is_streak is_answered explanation'.split()

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_is_answered(self, obj):
        return True

    @extend_schema_field(OpenApiTypes.STR)
    def get_explanation(self, obj):
        answer = self.context.get('answer')
        return answer.question.explanation

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_is_correct(self, obj):
        answer = self.context.get('answer', None)
        if answer.is_correct:
            return True
        return False

    @extend_schema_field(OpenApiTypes.INT)
    def get_correct_answers(self, obj):
        return obj.correct_answers

    @extend_schema_field(OpenApiTypes.INT)
    def get_wrong_answers(self, obj):
        return obj.wrong_answers

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_is_streak(self, obj):
        if obj.streak >= 5:
            return True
        return False

    @extend_schema_field(OpenApiTypes.INT)
    def get_streak(self, obj):
        return obj.streak


class FinishTestSerializer(serializers.ModelSerializer):
    percent = serializers.SerializerMethodField()
    message = serializers.SerializerMethodField()
    passed = serializers.SerializerMethodField()
    points = serializers.SerializerMethodField()
    is_test = serializers.SerializerMethodField()

    class Meta:
        model = UserLessonProgress
        fields = 'id percent message passed points is_test'.split()

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_is_test(self, obj):
        return True

    def calculate_lesson_completion_percentage(self, correct_answers):
        question_count = self.context.get('question_count')
        total_questions = int(question_count)
        if total_questions > 0:
            return int((correct_answers / total_questions) * 100)
        else:
            return 0

    def determine_completion_status(self, percentage):
        if percentage >= 80:
            return 'Отлично'
        elif percentage >= 51:
            return 'Хорошо'
        elif percentage >= 0:
            return 'Неплохо'
        else:
            return 'Ошибка вычисления'

    @extend_schema_field(OpenApiTypes.INT)
    def get_percent(self, obj):
        percent = self.calculate_lesson_completion_percentage(obj.correct_answers)
        return percent

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_passed(self, obj):
        question_count = self.context.get('question_count')
        all_answers = int(obj.correct_answers + obj.wrong_answers)
        if all_answers < question_count:
            raise ValueError('Не на все вопросы был дан ответ')
        if all_answers >= question_count and obj.wrong_answers <= 2:
            return True
        return False

    @extend_schema_field(OpenApiTypes.INT)
    def get_points(self, obj):
        passed = self.get_passed(obj)
        if passed:
            if obj.passed:
                return 15
            return 30
        return 0

    @extend_schema_field(OpenApiTypes.STR)
    def get_message(self, obj):
        percent = self.calculate_lesson_completion_percentage(obj.correct_answers)
        message = self.determine_completion_status(percent)
        return message


class TestQuestionSerializer(serializers.ModelSerializer):
    questions = SerializerListQuestion(many=True)

    class Meta:
        model = Test
        fields = ('id', 'level', 'title', 'questions')


class FinishLessonSerializer(serializers.ModelSerializer):
    percent = serializers.SerializerMethodField()
    message = serializers.SerializerMethodField()
    passed = serializers.SerializerMethodField()
    points = serializers.SerializerMethodField()
    is_lesson = serializers.SerializerMethodField()

    class Meta:
        model = UserLessonProgress
        fields = 'id percent message passed points is_lesson'.split()

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_is_lesson(self, obj):
        return True

    def calculate_lesson_completion_percentage(self, correct_answers):
        question_count = self.context.get('question_count')
        total_questions = int(question_count)
        if total_questions > 0:
            return int((correct_answers / total_questions) * 100)
        else:
            return 0

    def determine_completion_status(self, percentage):
        if percentage >= 80:
            return 'Отлично'
        elif percentage >= 51:
            return 'Хорошо'
        elif percentage >= 0:
            return 'Неплохо'
        else:
            return 'Ошибка вычисления'

    @extend_schema_field(OpenApiTypes.INT)
    def get_percent(self, obj):
        percent = self.calculate_lesson_completion_percentage(obj.correct_answers)
        return percent

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_passed(self, obj):
        question_count = self.context.get('question_count')
        all_answers = int(obj.correct_answers + obj.wrong_answers)
        if all_answers >= question_count:
            return True
        raise ValueError('Не на все вопросы был дан ответ')

    @extend_schema_field(OpenApiTypes.INT)
    def get_points(self, obj):
        passed = self.get_passed(obj)
        if passed:
            if obj.passed:
                return 5
            return 15
        return 0

    @extend_schema_field(OpenApiTypes.STR)
    def get_message(self, obj):
        percent = self.calculate_lesson_completion_percentage(obj.correct_answers)
        message = self.determine_completion_status(percent)
        return message


class ResponseTestAnswering(serializers.ModelSerializer):
    is_correct = serializers.SerializerMethodField()
    correct_answers = serializers.SerializerMethodField()
    wrong_answers = serializers.SerializerMethodField()
    is_streak = serializers.SerializerMethodField()
    streak = serializers.SerializerMethodField()
    is_answered = serializers.SerializerMethodField()
    explanation = serializers.SerializerMethodField()

    class Meta:
        model = UserLessonProgress
        fields = 'is_correct correct_answers wrong_answers streak is_streak is_answered explanation'.split()

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_is_answered(self, obj):
        return True

    @extend_schema_field(OpenApiTypes.STR)
    def get_explanation(self, obj):
        answer = self.context.get('answer')
        return answer.question.explanation

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_is_correct(self, obj):
        answer = self.context.get('answer', None)
        if answer.is_correct:
            return True
        return False

    @extend_schema_field(OpenApiTypes.INT)
    def get_correct_answers(self, obj):
        return obj.correct_answers

    @extend_schema_field(OpenApiTypes.INT)
    def get_wrong_answers(self, obj):
        return obj.wrong_answers

    @extend_schema_field(OpenApiTypes.BOOL)
    def get_is_streak(self, obj):
        if obj.streak >= 5:
            return True
        return False

    @extend_schema_field(OpenApiTypes.INT)
    def get_streak(self, obj):
        return obj.streak
