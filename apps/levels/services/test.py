from django.db.models import Count

from apps.levels.models import UserTestProgress, Test, UserLessonProgress, UserLevelProgress, UserTestQuestionProgress
from apps.levels.services.mixins import AnswerMixin


class TestService:
    model = Test
    test_progress_model = UserTestProgress
    lesson_progress_model = UserLessonProgress

    @classmethod
    def get_test(cls, **kwargs):
        try:
            return cls.model.objects.annotate(
                lesson_counts = Count('level__lesson')
            ).get(**kwargs)
        except Test.DoesNotExist:
            raise ValueError('Тест не найден')

    @classmethod
    def get_test_progress(cls, **kwargs):
        try:
            return cls.test_progress_model.objects.get(**kwargs)
        except cls.test_progress_model.DoesNotExist:
            return []

    @classmethod
    def filter_lesson_progress(cls, **kwargs):
        return cls.lesson_progress_model.objects.filter(**kwargs).values_list('lesson__order')


class TestQuestionService(AnswerMixin):
    test_model = Test
    level_progress_model = UserLevelProgress
    test_progress_model = UserTestProgress
    question_progress_model = UserTestQuestionProgress

    @classmethod
    def get_test(cls, **kwargs):
        try:
            return cls.test_model.objects.prefetch_related('questions__answers').get(**kwargs)
        except cls.test_model.DoesNotExist:
            raise ValueError('Обьект теста существует')

    @classmethod
    def get_or_create_level_progress(cls, **kwargs):
        try:
            return cls.level_progress_model.objects.get_or_create(**kwargs)
        except cls.level_progress_model.DoesNotExist:
            raise ValueError('Прогресс пользователя по уровню не найден')

    @classmethod
    def get_or_create_test_progress(cls, **kwargs):
        try:
            return cls.test_progress_model.objects.select_related(
                'user_level_progress__user__profile'
            ).get_or_create(**kwargs)
        except cls.test_progress_model.DoesNotExist:
            raise ValueError('Прогресс пользователя по тестам не найден')

    @classmethod
    def filter_question_progress(cls, **kwargs):
        return cls.question_progress_model.objects.filter(**kwargs).values_list('question_id', flat = True)

    @classmethod
    def create_question_progress(cls, **kwargs):
        cls.question_progress_model.objects.create(**kwargs)


class FinishTestService:
    test_progress_model = UserTestProgress
    test_model = Test
    question_progress_model = UserTestQuestionProgress

    @classmethod
    def get_test_progress(cls, **kwargs):
        try:
            return cls.test_progress_model.objects.select_related(
                'user_level_progress__user__profile',
            ).get(**kwargs)
        except UserTestProgress.DoesNotExist:
            raise ValueError('Прогресс по тесту не найден')

    @classmethod
    def filter_question_progress(cls, **kwargs):
        return cls.question_progress_model.objects.filter(**kwargs)

    @classmethod
    def question_count(cls, **kwargs):
        try:
            return cls.test_model.objects.get(**kwargs).questions.count()
        except cls.test_model.DoesNotExist:
            raise ValueError('Обьект теста не найден!')

    @classmethod
    def update_first_passed(cls, profile, test_progress):
        profile.point_count += 30
        profile.total_points += 30
        profile.completed_test += 1
        test_progress.user_level_progress.passed = True
        test_progress.user_level_progress.save()
        test_progress.passed = True
        test_progress.save()
        profile.save()

    @classmethod
    def update_re_passing(cls, profile):
        profile.point_count += 15
        profile.total_points += 15
        profile.save()

    @classmethod
    def finish_update(cls, test_progress, question_progress):
        test_progress.correct_answers = 0
        test_progress.streak = 0
        test_progress.wrong_answers = 0
        test_progress.save()
        question_progress.delete()


class ResetTestProgressService:
    test_progress_model = UserTestProgress
    question_progress_model = UserTestQuestionProgress

    @classmethod
    def get_test_progress(cls, **kwargs):
        try:
            return cls.test_progress_model.objects.get(**kwargs)
        except cls.test_progress_model.DoesNotExist:
            raise ValueError('Прогресс по тесту не был найден')

    @classmethod
    def filter_question_progress(cls, **kwargs):
        return cls.question_progress_model.objects.filter(**kwargs)

    @classmethod
    def reset_progress(cls, test_progress, question_progress):
        question_progress.delete()
        test_progress.correct_answers = 0
        test_progress.streak = 0
        test_progress.wrong_answers = 0
        test_progress.save()
