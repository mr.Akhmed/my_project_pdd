from django.contrib import admin
from django.contrib.admin.options import IS_POPUP_VAR
from django.core.exceptions import ValidationError
from django.forms import BaseInlineFormSet
from django.utils.safestring import mark_safe

from .models import (Level, Question, Answer,
                     UserLevelProgress, Test, Lesson, UserTestProgress, UserLessonProgress, UserQuestionProgress,
                     UserTestQuestionProgress)


class AnswerInlineFormSet(BaseInlineFormSet):
    """
    Расширенная Валидация в FormSet
    """

    def clean(self):
        super().clean()
        has_correct_answer = any(
            form.cleaned_data.get('is_correct', False)
            for form in self.forms
            if not form.cleaned_data.get('DELETE', False)
        )
        has_incorrect_answer = any(
            not form.cleaned_data.get('is_correct', True)
            for form in self.forms
            if not form.cleaned_data.get('DELETE', False)
        )

        if not has_correct_answer:
            raise ValidationError('Хотя бы один ответ должен быть отмечен как правильный.')

        if not has_incorrect_answer:
            raise ValidationError('Хотя бы один ответ должен быть отмечен как неправильный.')


class LessonInline(admin.TabularInline):
    verbose_name_plural = 'Добавить уроки'
    verbose_name = ''
    fields = ('level', 'title_ru', 'title_ky', 'order')
    readonly_fields = ('order',)
    model = Lesson


@admin.register(Test)
class TestInline(admin.ModelAdmin):
    fieldsets = (
        ('Тест', {
            'description': "Сначала создайте тест, после чего вы сможете добавить вопросы к нему!",
            'fields': ('level', 'title_ru', 'title_ky', 'questions'),
        }),
    )

    def get_fieldsets(self, request, obj=None):
        if obj:
            return (
                ('Тест', {
                    'fields': ('level', 'title_ru', 'title_ky', 'questions'),
                }),
            )
        return (
            ('Тест', {
                'description': "Сначала создайте тест, после чего вы сможете добавить вопросы к нему!",
                'fields': ('level', 'title_ru', 'title_ky'),
            }),
        )

    def response_add(self, request, obj, post_url_continue=None):
        if "_addanother" not in request.POST and IS_POPUP_VAR not in request.POST:
            request.POST = request.POST.copy()
            request.POST["_continue"] = 1
        return super().response_add(request, obj, post_url_continue)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "questions":
            obj_id = request.resolver_match.kwargs.get('object_id')
            if obj_id:
                test = Test.objects.get(id=obj_id)
                kwargs["queryset"] = Question.objects.filter(lesson__level=test.level)
            else:
                kwargs["queryset"] = Question.objects.none()
        return super().formfield_for_manytomany(db_field, request, **kwargs)


@admin.register(Level)
class LevelAdmin(admin.ModelAdmin):
    list_display = ('id', 'name_ru', 'name_ky', 'order',)
    list_display_links = ('id', 'name_ru')
    actions_on_top = False
    fields = ('name_ru', 'name_ky', 'description_ru', 'description_ky', 'order')

    readonly_fields = ('order',)
    inlines = [LessonInline]


class AnswerInline(admin.TabularInline):
    fields = ('question', 'text_ru', 'text_ky', 'is_correct')
    verbose_name_plural = 'Добавить ответы'
    verbose_name = ''
    formset = AnswerInlineFormSet
    max_num = 4
    model = Answer


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    actions_on_top = False
    inlines = [AnswerInline]
    list_filter = ('lesson',)
    fields = ('lesson', 'image', 'get_image', 'title_ru', 'title_ky', 'explanation_ru', 'explanation_ky', 'order')
    readonly_fields = ('order', 'get_image')

    list_display = ('lesson', 'title_ru', 'order',)

    def get_image(self, obj):
        return mark_safe(f'<img src={obj.image.url} width="250" height="150">')

    get_image.short_description = 'Изображение'
