from .models import Rank
from modeltranslation.translator import register, TranslationOptions


@register(Rank)
class RankTranslation(TranslationOptions):
    fields = ('title',)
    required_languages = ('ru', 'ky')
