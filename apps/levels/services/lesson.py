from apps.levels.models import Lesson, UserLevelProgress, UserLessonProgress, UserQuestionProgress, Question
from apps.levels.services.mixins import AnswerMixin


class LessonService:
    """
        Сервис для получения списка уроков
    """
    model = Lesson
    progress_model = UserLevelProgress

    @classmethod
    def filter_lesson(cls, **kwargs):
        return cls.model.objects.select_related('level').filter(**kwargs).order_by('order')

    @classmethod
    def get_or_create(cls, **kwargs):
        return cls.progress_model.objects.get_or_create(**kwargs)

    @classmethod
    def filter_progress(cls, userlevelprogress, **filters):
        return userlevelprogress.userlessonprogress.filter(**filters).values_list('lesson__order', flat = True)


class LessonQuestionService(AnswerMixin):
    """
        Сервис для процесса прохождения урока
    """
    question_model = Question
    level_progress_model = UserLevelProgress
    lesson_progress_model = UserLessonProgress
    question_progress_model = UserQuestionProgress

    @classmethod
    def filter_question(cls, **kwargs):
        return cls.question_model.objects.prefetch_related('answers').filter(**kwargs).order_by('order')

    @classmethod
    def get_or_create_level_progress(cls, **kwargs):
        try:
            return cls.level_progress_model.objects.get_or_create(**kwargs)
        except cls.level_progress_model.DoesNotExist:
            raise ValueError('Прогресс пользователя по уровню не найден')

    @classmethod
    def get_or_create_lesson_progress(cls, **kwargs):
        try:
            return cls.lesson_progress_model.objects.select_related(
                'user_level_progress__user__profile',
                'user_level_progress__user',
            ).get_or_create(**kwargs)
        except cls.level_progress_model.DoesNotExist:
            raise ValueError('Прогресс пользователя по уроку не найден')

    @classmethod
    def get_profile(cls, userlessonprogress):
        return userlessonprogress.user_level_progress.user.profile

    @classmethod
    def filter_answer_progress(cls, **kwargs):
        return cls.question_progress_model.objects.filter(**kwargs).values_list('question_id', flat = True)

    @classmethod
    def create_question_progress(cls, **kwargs):
        cls.question_progress_model.objects.create(**kwargs)


class FinishLessonService:
    lesson_progress_model = UserLessonProgress
    question_progress_model = UserQuestionProgress
    question_model = Question

    @classmethod
    def get_lesson_progress(cls, **kwargs):
        try:
            return cls.lesson_progress_model.objects.select_related('user_level_progress__user__profile', ).get(
                **kwargs
            )
        except cls.lesson_progress_model.DoesNotExist:
            raise ValueError('Прогресс по урокам не найден!')

    @classmethod
    def filter_question_progress(cls, **kwargs):
        return cls.question_progress_model.objects.filter(**kwargs)

    @classmethod
    def question_count(cls, **kwargs):
        return cls.question_model.objects.filter(**kwargs).count()

    @classmethod
    def update_first_passed(cls, profile):
        profile.point_count += 15
        profile.total_points += 15
        profile.completed_lesson += 1
        profile.save()

    @classmethod
    def update_re_passing(cls, profile):
        profile.point_count += 5
        profile.total_points += 5
        profile.save()

    @classmethod
    def finish_update(cls, user_lesson_progress, question_progress):
        user_lesson_progress.correct_answers = 0
        user_lesson_progress.streak = 0
        user_lesson_progress.wrong_answers = 0
        user_lesson_progress.passed = True
        user_lesson_progress.save()
        question_progress.delete()


class ResetLessonProgressService:
    lesson_progress_model = UserLessonProgress
    question_progress_model = UserQuestionProgress

    @classmethod
    def get_lesson_progress(cls, **kwargs):
        try:
            return cls.lesson_progress_model.objects.get(**kwargs)
        except cls.lesson_progress_model.DoesNotExist:
            raise ValueError('Прогресс по урокам не найден')

    @classmethod
    def filter_question_progress(cls, **kwargs):
        return cls.question_progress_model.objects.filter(**kwargs)

    @classmethod
    def reset_progress(cls, question_progress, lesson_progress):
        question_progress.delete()
        lesson_progress.correct_answers = 0
        lesson_progress.streak = 0
        lesson_progress.wrong_answers = 0
        lesson_progress.save()
