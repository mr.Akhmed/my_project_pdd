from django.urls import path
from apps.video_library.views import (CategoryView, SubCategoryVideosView)

urlpatterns = [
    path('categories/', CategoryView.as_view(), name='category-list'),
    path('subcategories/<int:id>/videos/', SubCategoryVideosView.as_view(), name='subcategory-videos'),
]
