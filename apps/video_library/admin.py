import requests
from django import forms
from django.contrib import admin
from django.core.exceptions import ValidationError
from django.utils.safestring import mark_safe
from modeltranslation.admin import TranslationAdmin

from .models import Category, Video, SubCategory


class VideoInline(admin.TabularInline):
    model = Video
    verbose_name_plural = 'Добавить видеоуроки'
    verbose_name = ''
    extra = 1
    fields = ('title_ru', 'title_ky', 'video_url_ru', 'video_url_ky')


class SubCategoriesInline(admin.TabularInline):
    model = SubCategory
    verbose_name_plural = 'Добавить подкатегории'
    verbose_name = ''
    extra = 1
    fields = ('title_ru', 'title_ky', 'image')
    inlines = [VideoInline]


class CategoryAdmin(TranslationAdmin):
    list_display = ('title',)
    inlines = [SubCategoriesInline]


class SubCategoryAdmin(TranslationAdmin):
    list_filter = ('category',)
    list_display_links = ('id', 'get_image',)
    list_display = ('id', 'get_image', 'category',)
    readonly_fields = ('get_image',)
    inlines = [VideoInline]

    def get_image(self, obj):
        return mark_safe(f'<img src={obj.image.url} width="150" height="150">')

    get_image.short_description = 'Изображение'


class VideoForm(forms.ModelForm):
    class Meta:
        model = Video
        fields = ('title_ru', 'title_ky', 'subcategory', 'video_url_ru', 'video_url_ky')

    def clean_video_url(self, url):
        video_url = url
        video_url_plus = 'https://www.youtube.com/oembed?url=' + video_url
        try:
            response = requests.head(video_url_plus)
            response.raise_for_status()
        except requests.exceptions.RequestException as e:
            raise ValidationError('Ссылка на YouTube недействительна!')
        return video_url

    def clean_video_url_ky(self):
        return self.clean_video_url(self.cleaned_data['video_url_ky'])

    def clean_video_url_ru(self):
        return self.clean_video_url(self.cleaned_data['video_url_ru'])


class VideoAdmin(TranslationAdmin):
    list_display = ('title_ru', 'subcategory', 'video_url_ru')
    list_filter = ('subcategory',)
    form = VideoForm


admin.site.register(Category, CategoryAdmin)
admin.site.register(SubCategory, SubCategoryAdmin)
admin.site.register(Video, VideoAdmin)
